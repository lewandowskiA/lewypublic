#include <iostream>
#include "../include/Neva.hpp"

void Neva::meow()
{
    std::cout << "MMMEEEOOOOOWWWW!!!!!\n";
}

void Neva::update(MouseEvents ev)
{
    std::cout << "Neva received event:";

    switch (ev)
    {
    case MouseEvents::move:
        std::cout << "mouse moved!\n";
        break;
    case MouseEvents::noise:
        std::cout << "mouse made some noise!\n";
        break;
    }
}

Neva::Neva(int mWeight,...)
:mWeight(mWeight);
{
   
}