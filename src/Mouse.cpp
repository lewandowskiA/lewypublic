#include <iostream>
#include <algorithm>
#include "../include/Mouse.hpp"
#include "../include/Cat.hpp"

Mouse::Mouse()
    : observers{}
{
}

void Mouse::registerObserver(IObserver *o)
{
    observers.push_back(o);
}

void Cirrus::unregisterObserver(IObserver *o)
{
    vec.erase(std::remove(vec.begin(), vec.end(), o), vec.end());
}

void Mouse::notify(MouseEvents ev)
{
    std::cout << "Mouse notifies about event\n";

    for (const auto &o : observers)
    {
        o->update(ev);
    }
}
