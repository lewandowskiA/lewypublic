#include <iostream>
#include <string>

//Factory
#include "../include/CatTypes.hpp"
#include "../include/Cat.hpp"
#include "../include/CatFactory.hpp"

//Observer
#include "../include/IObservable.hpp"
#include "../include/IObserver.hpp"
#include "../include/MouseEvents.hpp"
#include "../include/Mouse.hpp"

#include "../include/Siberian.hpp"
#include "../include/Neva.hpp"

int main()
{    
    //Subject / Observable
    Mouse mouse;
    Cirrus cirrus;

    //Observer
    IObserver *cat1 = new Siberian(4, 4, 5, 6, 7, 9);
    IObserver *cat2 = new Neva(5, 6, 7, 8, 8, 5);
    IObserver *watcherFirst = new Watcher();
    IObserver *watcherSecond = new Watcher();

    //Observer registers to Observable
    mouse.registerObserver(cat1);
    mouse.registerObserver(cat2);

    mouse.notify(MouseEvents::move);
    mouse.notify(MouseEvents::noise);

    mouse.unregisterObserver(cat1);

    mouse.notify(MouseEvents::move);
    mouse.notify(MouseEvents::noise);

    std::cout << "\nxxx\n";

    cirrus.registerObserver(watcherFirst);
    cirrus.registerObserver(watcherSecond);

    cirrus.notify(CirrusEvents::Monkey);
    cirrus.notify(CirrusEvents::Zebra);
    cirrus.notify(CirrusEvents::Elephant);

    cirrus.unregisterObserver(watcherFirst);

    cirrus.notify(CirrusEvents::Monkey);
    cirrus.notify(CirrusEvents::Zebra);
    cirrus.notify(CirrusEvents::Elephant);

    return 0;
}

/*
Tasks to consider:

  
   x Extend constructor of class Siberian and Neva and made them taking different number of parameters.
    x Modify Factory when objects are still created in similar way


  x  make Observer registering for specific kind of event
  x  make Observable notifying Observers per events (not all Observers about all events)
  x  implement method unregisterObserver. Remember about correct memory management
   
    ** make observers vector storing smart pointers.
*/