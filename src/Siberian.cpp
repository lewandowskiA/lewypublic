#include <iostream>
#include "../include/Siberian.hpp"

void Siberian::meow()
{
    std::cout << "meoow....\n";
}

void Siberian::update(MouseEvents ev)
{
    std::cout << "Siberian received event:";

    switch (ev)
    {
        case MouseEvents::move :
        std::cout << "mouse moved!\n";
        break;
        case MouseEvents::noise :
        std::cout << "mouse made some noise!\n";
        break;
    }
}

Siberian::Siberian(int mWeight,...)
:mWeight(mWeight)
{
    
}
