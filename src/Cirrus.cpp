#include <iostream>
#include <algorithm>
#include "../include/Cirrus.hpp"
#include "../include/Watcher.hpp"

Cirrus::Cirrus()
    : observers{}
{
}

void Cirrus::registerObserver(IObserver *o)
{
    observers.push_back(o);
}


 void Cirrus::unregisterObserver(IObserver * o)
    {
        vec.erase(std::remove(vec.begin(), vec.end(), o), vec.end());
    }


void Cirrus::notify(CirrusEvents ev)
{
    std::cout << "Welcome welcome in Cirrus!\n";

    for (const auto &o : observers)
    {
        o->update(ev);
    }

   
}
