#include "../include/CatFactorySiberian.hpp"
#include "../include/Siberian.hpp"

Cat *CatFactorySiberian::createCat(int a)
{
    return new Siberian(a);
}
