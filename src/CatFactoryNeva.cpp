#include "../include/CatFactoryNeva.hpp"
#include "../include/Neva.hpp"

Cat *CatFactoryNeva::createCat(int a)
{
    return new Neva(a);
}
