#ifndef CATFACTORY_HPP
#define CATFACTORY_HPP

#include "../include/CatTypes.hpp"
#include "../include/Cat.hpp"

class CatFactory
{
  public:
    static virtual Cat *createCat(int a) = 0;

  protected:
  private:
};

class CatFactoryNeva : public CatFactory
{
  public:
    static Cat *createCat(int a) override;
};

class CatFactorySiberian : public CatFactory
{
  public:
    static Cat *createCat(int a) override;
};

#endif
