#ifndef WATCHER_HPP
#define WATCHER_HPP


#include "IObserver.hpp"

class Watcher :  public IObserver
{
  public:
    virtual ~Watcher = default;
    void update(CirrusEvents ev) override;

  protected:
  private:
   
};

#endif