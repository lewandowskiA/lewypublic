#ifndef MOUSEEVENTS_HPP
#define MOUSEEVENTS_HPP

enum class MouseEvents
{
    move,
    noise
};

#endif
