#ifndef IOBSERVABLE_HPP
#define IOBSERVABLE_HPP

#include "MouseEvents.hpp"
#include "IObserver.hpp"

class IObservable {
public:
    virtual ~IObservable() = default;
    virtual void registerObserver(IObserver* o) = 0;
    virtual void notify(MouseEvents ev) = 0;
protected:
private:
};

#endif
