#ifndef CATFACTORYSIBERIAN_HPP
#define CATFACTORYSIBERIAN_HPP

#include "../include/CatFactory.hpp"

class CatFactorySiberian : public CatFactory
{
  public:
    static Cat *createCat(int a) override;
};

#endif
