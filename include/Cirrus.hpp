#ifndef CIRRUS_HPP
#define CIRRUS_HPP
#include <memory>
#include <vector>
#include "IObserver.hpp"
#include "IObservable.hpp"

class Cirrus : public IObservable
{
  public:
    Cirrus();
    void registerObserver(IObserver *o) override;
    void unregisterObserver(IObserver *o) override;
    void notify(CirrusEvents ev) override;

  protected:
  private:
    std::vector<std::shared_ptr<IObserver>> observers;
};

#endif
