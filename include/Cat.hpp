#ifndef CAT_HPP
#define CAT_HPP
#include <cstdarg>
#include <string>

class Cat
{
public:
    virtual ~Cat() = default;
    virtual void meow() = 0;
protected:
private:
};

#endif
