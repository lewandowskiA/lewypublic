#ifndef CIRRUSEVENTS_HPP
#define CIRRUSEVENTS_HPP

enum class CirrusEvents
{
    Elephant,
    Zebra,
    Monkey
};

#endif