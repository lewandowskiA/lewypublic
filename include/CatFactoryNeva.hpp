#ifndef CATFACTORYNEVA_HPP
#define CATFACTORYNEVA_HPP

#include "../include/CatFactory.hpp"

class CatFactoryNeva : public CatFactory
{
  public:
    static Cat *createCat(int a) override;
};

#endif
