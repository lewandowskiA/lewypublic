#ifndef IOBSERVER_HPP
#define IOBSERVER_HPP

#include "MouseEvents.hpp"
#include "CirrusEvents.hpp"

class IObserver
{
public:
    virtual ~IObserver() = default;
    virtual void update(MouseEvents ev) = 0;
    virtual void update(CirrusEvents ev) = 0;
protected:
private:
};

#endif
