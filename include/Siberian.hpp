#ifndef SIBERIAN_HPP
#define SIBERIAN_HPP

#include "Cat.hpp"
#include "IObserver.hpp"

class Siberian : public Cat, public IObserver
{
  public:
    Siberian(int mWeight,...);
    virtual ~Siberian() = default;
    void meow() override;    
    void update(MouseEvents ev) override;

  protected:
  private:
   int mWeight;
};

#endif
