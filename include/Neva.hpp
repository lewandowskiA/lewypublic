#ifndef NEVA_HPP
#define NEVA_HPP

#include "Cat.hpp"
#include "IObserver.hpp"

class Neva : public Cat, public IObserver
{
  public:
    Neva(int mWeight, ...);
    virtual ~Neva() = default;
    void meow() override;
    void update(MouseEvents ev) override;

  protected:
  private:
   int mWeight;
};

#endif
