#ifndef MOUSE_HPP
#define MOUSE_HPP
#include <memory>
#include <vector>
#include "IObserver.hpp"
#include "IObservable.hpp"

class Mouse : public IObservable
{
  public:
    Mouse();
    void registerObserver(IObserver *o) override;
    void unregisterObserver(IObserver *o) override;
    void notify(MouseEvents ev) override;

  protected:
  private:
   // std::vector<IObserver *> observers;
    std::vector<std::shared_ptr<IObserver>> observers;
};

#endif
